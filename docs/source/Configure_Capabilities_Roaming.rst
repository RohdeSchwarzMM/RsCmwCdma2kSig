Roaming
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:ROAMing:OCLass
	single: CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:ROAMing:HOME
	single: CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:ROAMing:SID
	single: CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:ROAMing:NID

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:ROAMing:OCLass
	CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:ROAMing:HOME
	CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:ROAMing:SID
	CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:ROAMing:NID



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Capabilities_.Roaming.Roaming
	:members:
	:undoc-members:
	:noindex: