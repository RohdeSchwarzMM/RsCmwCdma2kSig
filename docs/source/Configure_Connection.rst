Connection
----------------------------------------





.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Connection.Connection
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Edau.rst