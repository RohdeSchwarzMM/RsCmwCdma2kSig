Msettings
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:MSETtings:MCC
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:MSETtings:PLCM
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:MSETtings:NMSI
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:MSETtings:UMRData

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:NETWork:MSETtings:MCC
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:MSETtings:PLCM
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:MSETtings:NMSI
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:MSETtings:UMRData



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Network_.Msettings.Msettings
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.network.msettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Network_Msettings_Imin.rst