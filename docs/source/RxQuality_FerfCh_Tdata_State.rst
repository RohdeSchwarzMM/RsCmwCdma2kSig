State
----------------------------------------





.. autoclass:: RsCmwCdma2kSig.Implementations.RxQuality_.FerfCh_.Tdata_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.ferfCh.tdata.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_FerfCh_Tdata_State_All.rst