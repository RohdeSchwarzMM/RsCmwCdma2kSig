Pdm
----------------------------------------





.. autoclass:: RsCmwCdma2kSig.Implementations.Call_.Pdm.Pdm
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.call.pdm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Call_Pdm_Send.rst
	Call_Pdm_Receive.rst