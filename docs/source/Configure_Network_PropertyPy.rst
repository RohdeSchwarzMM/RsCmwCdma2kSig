PropertyPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:PROPerty:PNOFfset
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:PROPerty:CLDTime
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:PROPerty:PRTimeout
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:PROPerty:LTOFfset
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:PROPerty:DLSavings
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:PROPerty:LATitude
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:PROPerty:LONGitude

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:NETWork:PROPerty:PNOFfset
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:PROPerty:CLDTime
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:PROPerty:PRTimeout
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:PROPerty:LTOFfset
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:PROPerty:DLSavings
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:PROPerty:LATitude
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:PROPerty:LONGitude



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Network_.PropertyPy.PropertyPy
	:members:
	:undoc-members:
	:noindex: