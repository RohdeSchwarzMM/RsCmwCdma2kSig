Cindicator
----------------------------------------





.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Network_.Cindicator.Cindicator
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.network.cindicator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Network_Cindicator_Cid.rst