MsInfo
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:TEST:MSINfo:ESN
	single: CONFigure:CDMA:SIGNaling<Instance>:TEST:MSINfo:MEID

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:TEST:MSINfo:ESN
	CONFigure:CDMA:SIGNaling<Instance>:TEST:MSINfo:MEID



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Test_.MsInfo.MsInfo
	:members:
	:undoc-members:
	:noindex: