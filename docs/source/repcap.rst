RepCaps
=========





Instance (Global)
----------------------------------------------------

.. code-block:: python

	# Setting:
	driver.repcap_instance_set(repcap.Instance.Inst1)
	# Range:
	Inst1 .. Inst16
	# All values (16x):
	Inst1 | Inst2 | Inst3 | Inst4 | Inst5 | Inst6 | Inst7 | Inst8
	Inst9 | Inst10 | Inst11 | Inst12 | Inst13 | Inst14 | Inst15 | Inst16

Indicator
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Indicator.Nr1
	# Values (2x):
	Nr1 | Nr2

IpAddress
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.IpAddress.Version4
	# Values (2x):
	Version4 | Version6

Path
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Path.Nr1
	# Values (2x):
	Nr1 | Nr2

Segment
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Segment.Nr1
	# Values (4x):
	Nr1 | Nr2 | Nr3 | Nr4

