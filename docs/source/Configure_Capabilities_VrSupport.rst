VrSupport
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:VRSupport:SCH
	single: CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:VRSupport:MSBits

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:VRSupport:SCH
	CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:VRSupport:MSBits



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Capabilities_.VrSupport.VrSupport
	:members:
	:undoc-members:
	:noindex: