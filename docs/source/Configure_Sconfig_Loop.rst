Loop
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:SCONfig:LOOP:FRATe
	single: CONFigure:CDMA:SIGNaling<Instance>:SCONfig:LOOP:PGENeration
	single: CONFigure:CDMA:SIGNaling<Instance>:SCONfig:LOOP:PATTern

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:SCONfig:LOOP:FRATe
	CONFigure:CDMA:SIGNaling<Instance>:SCONfig:LOOP:PGENeration
	CONFigure:CDMA:SIGNaling<Instance>:SCONfig:LOOP:PATTern



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Sconfig_.Loop.Loop
	:members:
	:undoc-members:
	:noindex: