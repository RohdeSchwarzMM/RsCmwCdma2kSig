Handoff
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:HANDoff:BCLass
	single: CONFigure:CDMA:SIGNaling<Instance>:HANDoff:CHANnel

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:HANDoff:BCLass
	CONFigure:CDMA:SIGNaling<Instance>:HANDoff:CHANnel



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Handoff.Handoff
	:members:
	:undoc-members:
	:noindex: