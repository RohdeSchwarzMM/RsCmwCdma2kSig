State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:SIGNaling<Instance>:SOPTion<Const_ServiceOption>:STATe

.. code-block:: python

	FETCh:CDMA:SIGNaling<Instance>:SOPTion<Const_ServiceOption>:STATe



.. autoclass:: RsCmwCdma2kSig.Implementations.Soption_.State.State
	:members:
	:undoc-members:
	:noindex: