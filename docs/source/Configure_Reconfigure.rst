Reconfigure
----------------------------------------





.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Reconfigure.Reconfigure
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.reconfigure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Reconfigure_Layer.rst