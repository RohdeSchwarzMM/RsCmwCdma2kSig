System
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:SYSTem:SID
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:SYSTem:PREVision
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:SYSTem:MPRevision
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:SYSTem:BSID

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:NETWork:SYSTem:SID
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:SYSTem:PREVision
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:SYSTem:MPRevision
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:SYSTem:BSID



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Network_.System.System
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.network.system.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Network_System_Awin.rst
	Configure_Network_System_Nwin.rst
	Configure_Network_System_Rwin.rst