Fch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:LAYer:CHANnel:FCH

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:LAYer:CHANnel:FCH



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Layer_.Channel_.Fch.Fch
	:members:
	:undoc-members:
	:noindex: