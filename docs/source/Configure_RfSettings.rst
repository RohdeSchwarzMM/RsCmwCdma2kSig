RfSettings
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:RFSettings:EATTenuation
	single: CONFigure:CDMA:SIGNaling<Instance>:RFSettings:BCLass
	single: CONFigure:CDMA:SIGNaling<Instance>:RFSettings:FREQuency
	single: CONFigure:CDMA:SIGNaling<Instance>:RFSettings:FLFRequency
	single: CONFigure:CDMA:SIGNaling<Instance>:RFSettings:RLFRequency
	single: CONFigure:CDMA:SIGNaling<Instance>:RFSettings:FOFFset
	single: CONFigure:CDMA:SIGNaling<Instance>:RFSettings:CHANnel

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:RFSettings:EATTenuation
	CONFigure:CDMA:SIGNaling<Instance>:RFSettings:BCLass
	CONFigure:CDMA:SIGNaling<Instance>:RFSettings:FREQuency
	CONFigure:CDMA:SIGNaling<Instance>:RFSettings:FLFRequency
	CONFigure:CDMA:SIGNaling<Instance>:RFSettings:RLFRequency
	CONFigure:CDMA:SIGNaling<Instance>:RFSettings:FOFFset
	CONFigure:CDMA:SIGNaling<Instance>:RFSettings:CHANnel



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.RfSettings.RfSettings
	:members:
	:undoc-members:
	:noindex: