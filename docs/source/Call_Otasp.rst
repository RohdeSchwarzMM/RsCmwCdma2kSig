Otasp
----------------------------------------





.. autoclass:: RsCmwCdma2kSig.Implementations.Call_.Otasp.Otasp
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.call.otasp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Call_Otasp_Send.rst
	Call_Otasp_Receive.rst