Receive
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALL:CDMA:SIGNaling<Instance>:OTASp:RECeive:WATermark
	single: CALL:CDMA:SIGNaling<Instance>:OTASp:RECeive:RESet

.. code-block:: python

	CALL:CDMA:SIGNaling<Instance>:OTASp:RECeive:WATermark
	CALL:CDMA:SIGNaling<Instance>:OTASp:RECeive:RESet



.. autoclass:: RsCmwCdma2kSig.Implementations.Call_.Otasp_.Receive.Receive
	:members:
	:undoc-members:
	:noindex: