Preconfigure
----------------------------------------





.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Preconfigure.Preconfigure
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.preconfigure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Preconfigure_Layer.rst