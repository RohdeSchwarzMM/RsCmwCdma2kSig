Half
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:SPEech:HALF:PERCent
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:SPEech:HALF

.. code-block:: python

	SENSe:CDMA:SIGNaling<Instance>:RXQuality:SPEech:HALF:PERCent
	SENSe:CDMA:SIGNaling<Instance>:RXQuality:SPEech:HALF



.. autoclass:: RsCmwCdma2kSig.Implementations.Sense_.RxQuality_.Speech_.Half.Half
	:members:
	:undoc-members:
	:noindex: