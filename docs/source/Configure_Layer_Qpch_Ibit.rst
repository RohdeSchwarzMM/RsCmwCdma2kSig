Ibit<Indicator>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.layer.qpch.ibit.repcap_indicator_get()
	driver.configure.layer.qpch.ibit.repcap_indicator_set(repcap.Indicator.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:LAYer:QPCH:IBIT<Indicator>

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:LAYer:QPCH:IBIT<Indicator>



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Layer_.Qpch_.Ibit.Ibit
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.layer.qpch.ibit.clone()