ScFading
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:CDMA:SIGNaling<Instance>:SCENario:SCFading:EXTernal
	single: ROUTe:CDMA:SIGNaling<Instance>:SCENario:SCFading:INTernal

.. code-block:: python

	ROUTe:CDMA:SIGNaling<Instance>:SCENario:SCFading:EXTernal
	ROUTe:CDMA:SIGNaling<Instance>:SCENario:SCFading:INTernal



.. autoclass:: RsCmwCdma2kSig.Implementations.Route_.Scenario_.ScFading.ScFading
	:members:
	:undoc-members:
	:noindex: