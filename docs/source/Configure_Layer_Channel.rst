Channel
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:LAYer:CHANnel:PICH
	single: CONFigure:CDMA:SIGNaling<Instance>:LAYer:CHANnel:PCH
	single: CONFigure:CDMA:SIGNaling<Instance>:LAYer:CHANnel:QPCH
	single: CONFigure:CDMA:SIGNaling<Instance>:LAYer:CHANnel:SYNC

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:LAYer:CHANnel:PICH
	CONFigure:CDMA:SIGNaling<Instance>:LAYer:CHANnel:PCH
	CONFigure:CDMA:SIGNaling<Instance>:LAYer:CHANnel:QPCH
	CONFigure:CDMA:SIGNaling<Instance>:LAYer:CHANnel:SYNC



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Layer_.Channel.Channel
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.layer.channel.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Layer_Channel_Fch.rst
	Configure_Layer_Channel_Sch.rst