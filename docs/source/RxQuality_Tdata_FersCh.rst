FersCh
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:CDMA:SIGNaling<Instance>:RXQuality:TDATa:FERSch
	single: STOP:CDMA:SIGNaling<Instance>:RXQuality:TDATa:FERSch
	single: ABORt:CDMA:SIGNaling<Instance>:RXQuality:TDATa:FERSch

.. code-block:: python

	INITiate:CDMA:SIGNaling<Instance>:RXQuality:TDATa:FERSch
	STOP:CDMA:SIGNaling<Instance>:RXQuality:TDATa:FERSch
	ABORt:CDMA:SIGNaling<Instance>:RXQuality:TDATa:FERSch



.. autoclass:: RsCmwCdma2kSig.Implementations.RxQuality_.Tdata_.FersCh.FersCh
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.tdata.fersCh.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_Tdata_FersCh_State.rst