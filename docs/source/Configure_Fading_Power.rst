Power
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:FADing:POWer:SIGNal
	single: CONFigure:CDMA:SIGNaling<Instance>:FADing:POWer:SUM

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:FADing:POWer:SIGNal
	CONFigure:CDMA:SIGNaling<Instance>:FADing:POWer:SUM



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Fading_.Power.Power
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.fading.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Fading_Power_Noise.rst