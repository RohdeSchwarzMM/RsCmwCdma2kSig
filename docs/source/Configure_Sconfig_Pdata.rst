Pdata
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:SCONfig:PDATa:ITIMer
	single: CONFigure:CDMA:SIGNaling<Instance>:SCONfig:PDATa:DTIMer

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:SCONfig:PDATa:ITIMer
	CONFigure:CDMA:SIGNaling<Instance>:SCONfig:PDATa:DTIMer



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Sconfig_.Pdata.Pdata
	:members:
	:undoc-members:
	:noindex: