Receive
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALL:CDMA:SIGNaling<Instance>:PDM:RECeive:WATermark
	single: CALL:CDMA:SIGNaling<Instance>:PDM:RECeive:RESet

.. code-block:: python

	CALL:CDMA:SIGNaling<Instance>:PDM:RECeive:WATermark
	CALL:CDMA:SIGNaling<Instance>:PDM:RECeive:RESet



.. autoclass:: RsCmwCdma2kSig.Implementations.Call_.Pdm_.Receive.Receive
	:members:
	:undoc-members:
	:noindex: