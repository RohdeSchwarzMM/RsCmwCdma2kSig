Speech
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:SPEech:THRoughput
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:SPEech:STATe

.. code-block:: python

	SENSe:CDMA:SIGNaling<Instance>:RXQuality:SPEech:THRoughput
	SENSe:CDMA:SIGNaling<Instance>:RXQuality:SPEech:STATe



.. autoclass:: RsCmwCdma2kSig.Implementations.Sense_.RxQuality_.Speech.Speech
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.rxQuality.speech.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_RxQuality_Speech_Blanked.rst
	Sense_RxQuality_Speech_Eight.rst
	Sense_RxQuality_Speech_Quarter.rst
	Sense_RxQuality_Speech_Half.rst
	Sense_RxQuality_Speech_Full.rst