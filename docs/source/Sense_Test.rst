Test
----------------------------------------





.. autoclass:: RsCmwCdma2kSig.Implementations.Sense_.Test.Test
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.test.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Test_Rx.rst