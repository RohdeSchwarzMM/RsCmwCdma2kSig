Mmonitor
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:MMONitor:ENABle

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:MMONitor:ENABle



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Mmonitor.Mmonitor
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.mmonitor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Mmonitor_IpAddress.rst