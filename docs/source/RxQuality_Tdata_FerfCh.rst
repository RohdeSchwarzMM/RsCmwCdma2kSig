FerfCh
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:CDMA:SIGNaling<Instance>:RXQuality:TDATa:FERFch
	single: STOP:CDMA:SIGNaling<Instance>:RXQuality:TDATa:FERFch
	single: ABORt:CDMA:SIGNaling<Instance>:RXQuality:TDATa:FERFch

.. code-block:: python

	INITiate:CDMA:SIGNaling<Instance>:RXQuality:TDATa:FERFch
	STOP:CDMA:SIGNaling<Instance>:RXQuality:TDATa:FERFch
	ABORt:CDMA:SIGNaling<Instance>:RXQuality:TDATa:FERFch



.. autoclass:: RsCmwCdma2kSig.Implementations.RxQuality_.Tdata_.FerfCh.FerfCh
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.tdata.ferfCh.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_Tdata_FerfCh_State.rst