Registration
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:REGistration:DBASed
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:REGistration:TBASed
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:REGistration:HOME
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:REGistration:FSID
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:REGistration:FNID
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:REGistration:PUP
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:REGistration:PDOWn
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:REGistration:PARameter

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:NETWork:REGistration:DBASed
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:REGistration:TBASed
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:REGistration:HOME
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:REGistration:FSID
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:REGistration:FNID
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:REGistration:PUP
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:REGistration:PDOWn
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:REGistration:PARameter



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Network_.Registration.Registration
	:members:
	:undoc-members:
	:noindex: