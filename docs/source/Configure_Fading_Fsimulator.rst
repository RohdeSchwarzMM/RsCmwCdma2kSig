Fsimulator
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:FADing:FSIMulator:ENABle
	single: CONFigure:CDMA:SIGNaling<Instance>:FADing:FSIMulator:STANdard
	single: CONFigure:CDMA:SIGNaling<Instance>:FADing:FSIMulator:KCONstant

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:FADing:FSIMulator:ENABle
	CONFigure:CDMA:SIGNaling<Instance>:FADing:FSIMulator:STANdard
	CONFigure:CDMA:SIGNaling<Instance>:FADing:FSIMulator:KCONstant



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Fading_.Fsimulator.Fsimulator
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.fading.fsimulator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Fading_Fsimulator_Restart.rst
	Configure_Fading_Fsimulator_Globale.rst
	Configure_Fading_Fsimulator_Iloss.rst