FersCh
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:RXQuality:FERSch:TOUT
	single: CONFigure:CDMA:SIGNaling<Instance>:RXQuality:FERSch:REPetition
	single: CONFigure:CDMA:SIGNaling<Instance>:RXQuality:FERSch:SCONdition
	single: CONFigure:CDMA:SIGNaling<Instance>:RXQuality:FERSch:FRAMes

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:RXQuality:FERSch:TOUT
	CONFigure:CDMA:SIGNaling<Instance>:RXQuality:FERSch:REPetition
	CONFigure:CDMA:SIGNaling<Instance>:RXQuality:FERSch:SCONdition
	CONFigure:CDMA:SIGNaling<Instance>:RXQuality:FERSch:FRAMes



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.RxQuality_.FersCh.FersCh
	:members:
	:undoc-members:
	:noindex: