LtOffset
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:SYSTem:LTOFfset:HEX
	single: CONFigure:CDMA:SIGNaling<Instance>:SYSTem:LTOFfset

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:SYSTem:LTOFfset:HEX
	CONFigure:CDMA:SIGNaling<Instance>:SYSTem:LTOFfset



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.System_.LtOffset.LtOffset
	:members:
	:undoc-members:
	:noindex: