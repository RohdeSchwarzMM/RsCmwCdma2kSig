State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:SIGNaling<Instance>:RXQuality:TDATa:FERFch:STATe

.. code-block:: python

	FETCh:CDMA:SIGNaling<Instance>:RXQuality:TDATa:FERFch:STATe



.. autoclass:: RsCmwCdma2kSig.Implementations.RxQuality_.Tdata_.FerfCh_.State.State
	:members:
	:undoc-members:
	:noindex: