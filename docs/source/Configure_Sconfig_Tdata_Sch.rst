Sch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:SCONfig:TDATa:SCH:PGENeration
	single: CONFigure:CDMA:SIGNaling<Instance>:SCONfig:TDATa:SCH:PATTern
	single: CONFigure:CDMA:SIGNaling<Instance>:SCONfig:TDATa:SCH:CBFRames
	single: CONFigure:CDMA:SIGNaling<Instance>:SCONfig:TDATa:SCH:TXON
	single: CONFigure:CDMA:SIGNaling<Instance>:SCONfig:TDATa:SCH:TXOFf

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:SCONfig:TDATa:SCH:PGENeration
	CONFigure:CDMA:SIGNaling<Instance>:SCONfig:TDATa:SCH:PATTern
	CONFigure:CDMA:SIGNaling<Instance>:SCONfig:TDATa:SCH:CBFRames
	CONFigure:CDMA:SIGNaling<Instance>:SCONfig:TDATa:SCH:TXON
	CONFigure:CDMA:SIGNaling<Instance>:SCONfig:TDATa:SCH:TXOFf



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Sconfig_.Tdata_.Sch.Sch
	:members:
	:undoc-members:
	:noindex: