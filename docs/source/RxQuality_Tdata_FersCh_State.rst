State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:SIGNaling<Instance>:RXQuality:TDATa:FERSch:STATe

.. code-block:: python

	FETCh:CDMA:SIGNaling<Instance>:RXQuality:TDATa:FERSch:STATe



.. autoclass:: RsCmwCdma2kSig.Implementations.RxQuality_.Tdata_.FersCh_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.tdata.fersCh.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_Tdata_FersCh_State_All.rst