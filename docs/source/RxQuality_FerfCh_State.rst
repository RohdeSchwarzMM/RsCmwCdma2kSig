State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:SIGNaling<Instance>:RXQuality:FERFch:STATe

.. code-block:: python

	FETCh:CDMA:SIGNaling<Instance>:RXQuality:FERFch:STATe



.. autoclass:: RsCmwCdma2kSig.Implementations.RxQuality_.FerfCh_.State.State
	:members:
	:undoc-members:
	:noindex: