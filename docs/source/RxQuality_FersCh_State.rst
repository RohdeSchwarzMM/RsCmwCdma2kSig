State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:CDMA:SIGNaling<Instance>:RXQuality:FERSch:STATe

.. code-block:: python

	FETCh:CDMA:SIGNaling<Instance>:RXQuality:FERSch:STATe



.. autoclass:: RsCmwCdma2kSig.Implementations.RxQuality_.FersCh_.State.State
	:members:
	:undoc-members:
	:noindex: