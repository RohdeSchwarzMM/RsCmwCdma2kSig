Scenario
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:CDMA:SIGNaling<Instance>:SCENario:SCELl
	single: ROUTe:CDMA:SIGNaling<Instance>:SCENario:HMODe
	single: ROUTe:CDMA:SIGNaling<Instance>:SCENario:HMLite
	single: ROUTe:CDMA:SIGNaling<Instance>:SCENario

.. code-block:: python

	ROUTe:CDMA:SIGNaling<Instance>:SCENario:SCELl
	ROUTe:CDMA:SIGNaling<Instance>:SCENario:HMODe
	ROUTe:CDMA:SIGNaling<Instance>:SCENario:HMLite
	ROUTe:CDMA:SIGNaling<Instance>:SCENario



.. autoclass:: RsCmwCdma2kSig.Implementations.Route_.Scenario.Scenario
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_ScFading.rst
	Route_Scenario_HmFading.rst