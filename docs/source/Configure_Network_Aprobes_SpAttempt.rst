SpAttempt
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:APRobes:SPATtempt:RSP
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:APRobes:SPATtempt:REQ

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:NETWork:APRobes:SPATtempt:RSP
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:APRobes:SPATtempt:REQ



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Network_.Aprobes_.SpAttempt.SpAttempt
	:members:
	:undoc-members:
	:noindex: