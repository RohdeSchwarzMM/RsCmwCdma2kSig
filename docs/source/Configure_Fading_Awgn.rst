Awgn
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:FADing:AWGN:ENABle
	single: CONFigure:CDMA:SIGNaling<Instance>:FADing:AWGN:SNRatio

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:FADing:AWGN:ENABle
	CONFigure:CDMA:SIGNaling<Instance>:FADing:AWGN:SNRatio



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Fading_.Awgn.Awgn
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.fading.awgn.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Fading_Awgn_Bandwidth.rst