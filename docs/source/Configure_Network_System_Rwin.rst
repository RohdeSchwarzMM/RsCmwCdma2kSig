Rwin
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:SYSTem:RWIN

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:NETWork:SYSTem:RWIN



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Network_.System_.Rwin.Rwin
	:members:
	:undoc-members:
	:noindex: