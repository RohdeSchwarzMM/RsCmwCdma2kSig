Pstrength
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:RXQuality:PSTRength:REPetition
	single: CONFigure:CDMA:SIGNaling<Instance>:RXQuality:PSTRength:URATe

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:RXQuality:PSTRength:REPetition
	CONFigure:CDMA:SIGNaling<Instance>:RXQuality:PSTRength:URATe



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.RxQuality_.Pstrength.Pstrength
	:members:
	:undoc-members:
	:noindex: