Noise
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:FADing:POWer:NOISe:TOTal
	single: CONFigure:CDMA:SIGNaling<Instance>:FADing:POWer:NOISe

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:FADing:POWer:NOISe:TOTal
	CONFigure:CDMA:SIGNaling<Instance>:FADing:POWer:NOISe



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Fading_.Power_.Noise.Noise
	:members:
	:undoc-members:
	:noindex: