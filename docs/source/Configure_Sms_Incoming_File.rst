File
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:SMS:INComing:FILE:INFO
	single: CONFigure:CDMA:SIGNaling<Instance>:SMS:INComing:FILE

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:SMS:INComing:FILE:INFO
	CONFigure:CDMA:SIGNaling<Instance>:SMS:INComing:FILE



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Sms_.Incoming_.File.File
	:members:
	:undoc-members:
	:noindex: