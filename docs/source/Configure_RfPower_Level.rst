Level
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:RFPower:LEVel:PICH
	single: CONFigure:CDMA:SIGNaling<Instance>:RFPower:LEVel:SYNC
	single: CONFigure:CDMA:SIGNaling<Instance>:RFPower:LEVel:PCH
	single: CONFigure:CDMA:SIGNaling<Instance>:RFPower:LEVel:FCH
	single: CONFigure:CDMA:SIGNaling<Instance>:RFPower:LEVel:SCH
	single: CONFigure:CDMA:SIGNaling<Instance>:RFPower:LEVel:QPCH
	single: CONFigure:CDMA:SIGNaling<Instance>:RFPower:LEVel:AWGN

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:RFPower:LEVel:PICH
	CONFigure:CDMA:SIGNaling<Instance>:RFPower:LEVel:SYNC
	CONFigure:CDMA:SIGNaling<Instance>:RFPower:LEVel:PCH
	CONFigure:CDMA:SIGNaling<Instance>:RFPower:LEVel:FCH
	CONFigure:CDMA:SIGNaling<Instance>:RFPower:LEVel:SCH
	CONFigure:CDMA:SIGNaling<Instance>:RFPower:LEVel:QPCH
	CONFigure:CDMA:SIGNaling<Instance>:RFPower:LEVel:AWGN



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.RfPower_.Level.Level
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfPower.level.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfPower_Level_Ocns.rst