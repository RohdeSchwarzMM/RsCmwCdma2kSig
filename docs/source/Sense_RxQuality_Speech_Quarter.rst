Quarter
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:SPEech:QUARter:PERCent
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:SPEech:QUARter

.. code-block:: python

	SENSe:CDMA:SIGNaling<Instance>:RXQuality:SPEech:QUARter:PERCent
	SENSe:CDMA:SIGNaling<Instance>:RXQuality:SPEech:QUARter



.. autoclass:: RsCmwCdma2kSig.Implementations.Sense_.RxQuality_.Speech_.Quarter.Quarter
	:members:
	:undoc-members:
	:noindex: