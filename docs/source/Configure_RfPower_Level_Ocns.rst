Ocns
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:RFPower:LEVel:OCNS

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:RFPower:LEVel:OCNS



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.RfPower_.Level_.Ocns.Ocns
	:members:
	:undoc-members:
	:noindex: