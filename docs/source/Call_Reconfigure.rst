Reconfigure
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALL:CDMA:SIGNaling<Instance>:REConfigure:STARt

.. code-block:: python

	CALL:CDMA:SIGNaling<Instance>:REConfigure:STARt



.. autoclass:: RsCmwCdma2kSig.Implementations.Call_.Reconfigure.Reconfigure
	:members:
	:undoc-members:
	:noindex: