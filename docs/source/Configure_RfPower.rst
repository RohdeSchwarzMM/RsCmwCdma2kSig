RfPower
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:RFPower:EXPected
	single: CONFigure:CDMA:SIGNaling<Instance>:RFPower:CDMA
	single: CONFigure:CDMA:SIGNaling<Instance>:RFPower:OUTPut
	single: CONFigure:CDMA:SIGNaling<Instance>:RFPower:EPMode
	single: CONFigure:CDMA:SIGNaling<Instance>:RFPower:MANual

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:RFPower:EXPected
	CONFigure:CDMA:SIGNaling<Instance>:RFPower:CDMA
	CONFigure:CDMA:SIGNaling<Instance>:RFPower:OUTPut
	CONFigure:CDMA:SIGNaling<Instance>:RFPower:EPMode
	CONFigure:CDMA:SIGNaling<Instance>:RFPower:MANual



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.RfPower.RfPower
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfPower_Level.rst
	Configure_RfPower_Ebnt.rst
	Configure_RfPower_Mode.rst