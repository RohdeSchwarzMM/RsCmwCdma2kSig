Test
----------------------------------------





.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Test.Test
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.test.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Test_MsInfo.rst