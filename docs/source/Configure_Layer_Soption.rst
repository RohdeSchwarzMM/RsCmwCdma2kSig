Soption
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:LAYer:SOPTion:FIRSt

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:LAYer:SOPTion:FIRSt



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Layer_.Soption.Soption
	:members:
	:undoc-members:
	:noindex: