Edau
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:CONNection:EDAU:ENABle
	single: CONFigure:CDMA:SIGNaling<Instance>:CONNection:EDAU:NSEGment
	single: CONFigure:CDMA:SIGNaling<Instance>:CONNection:EDAU:NID

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:CONNection:EDAU:ENABle
	CONFigure:CDMA:SIGNaling<Instance>:CONNection:EDAU:NSEGment
	CONFigure:CDMA:SIGNaling<Instance>:CONNection:EDAU:NID



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Connection_.Edau.Edau
	:members:
	:undoc-members:
	:noindex: