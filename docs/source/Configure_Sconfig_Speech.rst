Speech
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:SCONfig:SPEech:VCODer
	single: CONFigure:CDMA:SIGNaling<Instance>:SCONfig:SPEech:EDELay

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:SCONfig:SPEech:VCODer
	CONFigure:CDMA:SIGNaling<Instance>:SCONfig:SPEech:EDELay



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Sconfig_.Speech.Speech
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.sconfig.speech.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Sconfig_Speech_Evrc.rst