File
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:SMS:OUTGoing:FILE:INFO
	single: CONFigure:CDMA:SIGNaling<Instance>:SMS:OUTGoing:FILE

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:SMS:OUTGoing:FILE:INFO
	CONFigure:CDMA:SIGNaling<Instance>:SMS:OUTGoing:FILE



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Sms_.Outgoing_.File.File
	:members:
	:undoc-members:
	:noindex: