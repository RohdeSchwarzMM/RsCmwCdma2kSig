Evrc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:SCONfig:SPEech:EVRC:EOPoint
	single: CONFigure:CDMA:SIGNaling<Instance>:SCONfig:SPEech:EVRC:AERate
	single: CONFigure:CDMA:SIGNaling<Instance>:SCONfig:SPEech:EVRC:RREStriction
	single: CONFigure:CDMA:SIGNaling<Instance>:SCONfig:SPEech:EVRC:IVOCoder

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:SCONfig:SPEech:EVRC:EOPoint
	CONFigure:CDMA:SIGNaling<Instance>:SCONfig:SPEech:EVRC:AERate
	CONFigure:CDMA:SIGNaling<Instance>:SCONfig:SPEech:EVRC:RREStriction
	CONFigure:CDMA:SIGNaling<Instance>:SCONfig:SPEech:EVRC:IVOCoder



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Sconfig_.Speech_.Evrc.Evrc
	:members:
	:undoc-members:
	:noindex: