Moption
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:CSTatus:MOPTion:FCH
	single: CONFigure:CDMA:SIGNaling<Instance>:CSTatus:MOPTion:SCH

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:CSTatus:MOPTion:FCH
	CONFigure:CDMA:SIGNaling<Instance>:CSTatus:MOPTion:SCH



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Cstatus_.Moption.Moption
	:members:
	:undoc-members:
	:noindex: