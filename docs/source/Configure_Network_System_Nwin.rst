Nwin
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:SYSTem:NWIN

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:NETWork:SYSTem:NWIN



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Network_.System_.Nwin.Nwin
	:members:
	:undoc-members:
	:noindex: