Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:RFPower:MODE:AWGN

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:RFPower:MODE:AWGN



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.RfPower_.Mode.Mode
	:members:
	:undoc-members:
	:noindex: