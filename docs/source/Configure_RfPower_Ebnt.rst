Ebnt
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:RFPower:EBNT:FCH
	single: CONFigure:CDMA:SIGNaling<Instance>:RFPower:EBNT:SCH

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:RFPower:EBNT:FCH
	CONFigure:CDMA:SIGNaling<Instance>:RFPower:EBNT:SCH



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.RfPower_.Ebnt.Ebnt
	:members:
	:undoc-members:
	:noindex: