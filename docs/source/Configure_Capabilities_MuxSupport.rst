MuxSupport
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:MUXSupport:FWD
	single: CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:MUXSupport:REV

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:MUXSupport:FWD
	CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:MUXSupport:REV



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Capabilities_.MuxSupport.MuxSupport
	:members:
	:undoc-members:
	:noindex: