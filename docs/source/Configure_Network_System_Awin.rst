Awin
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:SYSTem:AWIN

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:NETWork:SYSTem:AWIN



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Network_.System_.Awin.Awin
	:members:
	:undoc-members:
	:noindex: