RsCmwCdma2kSig API Structure
========================================


.. rubric:: Global RepCaps

.. code-block:: python
	
	driver = RsCmwCdma2kSig('TCPIP::192.168.2.101::HISLIP')
	# Instance range: Inst1 .. Inst16
	rc = driver.repcap_instance_get()
	driver.repcap_instance_set(repcap.Instance.Inst1)

.. autoclass:: RsCmwCdma2kSig.RsCmwCdma2kSig
	:members:
	:undoc-members:
	:noindex:

.. rubric:: Subgroups

.. toctree::
	:maxdepth: 6
	:glob:

	Configure.rst
	Sense.rst
	Route.rst
	Source.rst
	Call.rst
	Soption.rst
	Clean.rst
	RxQuality.rst