FdrSupport
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:FDRSupport:FCH
	single: CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:FDRSupport:DCCH
	single: CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:FDRSupport:SCH

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:FDRSupport:FCH
	CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:FDRSupport:DCCH
	CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:FDRSupport:SCH



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Capabilities_.FdrSupport.FdrSupport
	:members:
	:undoc-members:
	:noindex: