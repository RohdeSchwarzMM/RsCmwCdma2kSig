Sms
----------------------------------------





.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Sms.Sms
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.sms.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Sms_Incoming.rst
	Configure_Sms_Outgoing.rst
	Configure_Sms_Info.rst
	Configure_Sms_Broadcast.rst