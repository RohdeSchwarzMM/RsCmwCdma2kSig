SoSupport
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:SOSupport:FFCH
	single: CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:SOSupport:RFCH

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:SOSupport:FFCH
	CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:SOSupport:RFCH



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Capabilities_.SoSupport.SoSupport
	:members:
	:undoc-members:
	:noindex: