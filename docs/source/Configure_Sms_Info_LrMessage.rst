LrMessage
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:SMS:INFO:LRMessage:RFLag
	single: CONFigure:CDMA:SIGNaling<Instance>:SMS:INFO:LRMessage

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:SMS:INFO:LRMessage:RFLag
	CONFigure:CDMA:SIGNaling<Instance>:SMS:INFO:LRMessage



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Sms_.Info_.LrMessage.LrMessage
	:members:
	:undoc-members:
	:noindex: