BsAddress
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CDMA:SIGNaling<Instance>:BSADdress:IPV<Const_IpV>

.. code-block:: python

	SENSe:CDMA:SIGNaling<Instance>:BSADdress:IPV<Const_IpV>



.. autoclass:: RsCmwCdma2kSig.Implementations.Sense_.BsAddress.BsAddress
	:members:
	:undoc-members:
	:noindex: