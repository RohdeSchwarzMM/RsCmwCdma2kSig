MsInfo
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:MSINfo:DNUMber
	single: CONFigure:CDMA:SIGNaling<Instance>:MSINfo:GECall
	single: CONFigure:CDMA:SIGNaling<Instance>:MSINfo:PREVision
	single: CONFigure:CDMA:SIGNaling<Instance>:MSINfo:MCC
	single: CONFigure:CDMA:SIGNaling<Instance>:MSINfo:NMSI
	single: CONFigure:CDMA:SIGNaling<Instance>:MSINfo:MSUPport
	single: CONFigure:CDMA:SIGNaling<Instance>:MSINfo:ESN
	single: CONFigure:CDMA:SIGNaling<Instance>:MSINfo:MEID
	single: CONFigure:CDMA:SIGNaling<Instance>:MSINfo:EIRP

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:MSINfo:DNUMber
	CONFigure:CDMA:SIGNaling<Instance>:MSINfo:GECall
	CONFigure:CDMA:SIGNaling<Instance>:MSINfo:PREVision
	CONFigure:CDMA:SIGNaling<Instance>:MSINfo:MCC
	CONFigure:CDMA:SIGNaling<Instance>:MSINfo:NMSI
	CONFigure:CDMA:SIGNaling<Instance>:MSINfo:MSUPport
	CONFigure:CDMA:SIGNaling<Instance>:MSINfo:ESN
	CONFigure:CDMA:SIGNaling<Instance>:MSINfo:MEID
	CONFigure:CDMA:SIGNaling<Instance>:MSINfo:EIRP



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.MsInfo.MsInfo
	:members:
	:undoc-members:
	:noindex: