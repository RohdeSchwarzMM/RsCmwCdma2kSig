FersCh
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:SIGNaling<Instance>:RXQuality:FERSch
	single: FETCh:CDMA:SIGNaling<Instance>:RXQuality:FERSch
	single: CALCulate:CDMA:SIGNaling<Instance>:RXQuality:FERSch

.. code-block:: python

	READ:CDMA:SIGNaling<Instance>:RXQuality:FERSch
	FETCh:CDMA:SIGNaling<Instance>:RXQuality:FERSch
	CALCulate:CDMA:SIGNaling<Instance>:RXQuality:FERSch



.. autoclass:: RsCmwCdma2kSig.Implementations.RxQuality_.FersCh.FersCh
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.fersCh.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_FersCh_State.rst