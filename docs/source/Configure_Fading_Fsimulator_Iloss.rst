Iloss
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:FADing:FSIMulator:ILOSs:MODE
	single: CONFigure:CDMA:SIGNaling<Instance>:FADing:FSIMulator:ILOSs:LOSS
	single: CONFigure:CDMA:SIGNaling<Instance>:FADing:FSIMulator:ILOSs:CSAMples

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:FADing:FSIMulator:ILOSs:MODE
	CONFigure:CDMA:SIGNaling<Instance>:FADing:FSIMulator:ILOSs:LOSS
	CONFigure:CDMA:SIGNaling<Instance>:FADing:FSIMulator:ILOSs:CSAMples



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Fading_.Fsimulator_.Iloss.Iloss
	:members:
	:undoc-members:
	:noindex: