Aprobes
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:APRobes:MODE
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:APRobes:NOFFset
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:APRobes:IOFFset
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:APRobes:PINCrement
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:APRobes:PPSequence

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:NETWork:APRobes:MODE
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:APRobes:NOFFset
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:APRobes:IOFFset
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:APRobes:PINCrement
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:APRobes:PPSequence



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Network_.Aprobes.Aprobes
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.network.aprobes.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Network_Aprobes_SpAttempt.rst