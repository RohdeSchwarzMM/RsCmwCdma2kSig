Capabilities
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:ENABle
	single: CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:BCSupport
	single: CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:SCSupport
	single: CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:TERMinal
	single: CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:GLOCation
	single: CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:WLL
	single: CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:AUTHentic
	single: CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:COMMon
	single: CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:RLPinfo

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:ENABle
	CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:BCSupport
	CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:SCSupport
	CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:TERMinal
	CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:GLOCation
	CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:WLL
	CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:AUTHentic
	CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:COMMon
	CONFigure:CDMA:SIGNaling<Instance>:CAPabilities:RLPinfo



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Capabilities.Capabilities
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.capabilities.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Capabilities_SoSupport.rst
	Configure_Capabilities_MuxSupport.rst
	Configure_Capabilities_Roaming.rst
	Configure_Capabilities_FdrSupport.rst
	Configure_Capabilities_VrSupport.rst