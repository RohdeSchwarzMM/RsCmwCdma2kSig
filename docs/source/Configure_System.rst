System
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:SYSTem:TSOurce
	single: CONFigure:CDMA:SIGNaling<Instance>:SYSTem:DATE
	single: CONFigure:CDMA:SIGNaling<Instance>:SYSTem:TIME
	single: CONFigure:CDMA:SIGNaling<Instance>:SYSTem:SYNC
	single: CONFigure:CDMA:SIGNaling<Instance>:SYSTem:ATIMe
	single: CONFigure:CDMA:SIGNaling<Instance>:SYSTem:LSEConds
	single: CONFigure:CDMA:SIGNaling<Instance>:SYSTem:DAYLight

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:SYSTem:TSOurce
	CONFigure:CDMA:SIGNaling<Instance>:SYSTem:DATE
	CONFigure:CDMA:SIGNaling<Instance>:SYSTem:TIME
	CONFigure:CDMA:SIGNaling<Instance>:SYSTem:SYNC
	CONFigure:CDMA:SIGNaling<Instance>:SYSTem:ATIMe
	CONFigure:CDMA:SIGNaling<Instance>:SYSTem:LSEConds
	CONFigure:CDMA:SIGNaling<Instance>:SYSTem:DAYLight



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.System.System
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.system.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_System_LtOffset.rst