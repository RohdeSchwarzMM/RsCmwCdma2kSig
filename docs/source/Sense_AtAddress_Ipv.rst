Ipv<IpAddress>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Version4 .. Version6
	rc = driver.sense.atAddress.ipv.repcap_ipAddress_get()
	driver.sense.atAddress.ipv.repcap_ipAddress_set(repcap.IpAddress.Version4)



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CDMA:SIGNaling<Instance>:ATADdress:IPV<IpAddress>

.. code-block:: python

	SENSe:CDMA:SIGNaling<Instance>:ATADdress:IPV<IpAddress>



.. autoclass:: RsCmwCdma2kSig.Implementations.Sense_.AtAddress_.Ipv.Ipv
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.atAddress.ipv.clone()