Elog
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CDMA:SIGNaling<Instance>:ELOG:LAST
	single: SENSe:CDMA:SIGNaling<Instance>:ELOG:ALL

.. code-block:: python

	SENSe:CDMA:SIGNaling<Instance>:ELOG:LAST
	SENSe:CDMA:SIGNaling<Instance>:ELOG:ALL



.. autoclass:: RsCmwCdma2kSig.Implementations.Sense_.Elog.Elog
	:members:
	:undoc-members:
	:noindex: