FerfCh
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:RXQuality:LIMit:FERFch:MFER
	single: CONFigure:CDMA:SIGNaling<Instance>:RXQuality:LIMit:FERFch:CLEVel

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:RXQuality:LIMit:FERFch:MFER
	CONFigure:CDMA:SIGNaling<Instance>:RXQuality:LIMit:FERFch:CLEVel



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.RxQuality_.Limit_.FerfCh.FerfCh
	:members:
	:undoc-members:
	:noindex: