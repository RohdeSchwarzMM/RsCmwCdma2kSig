Layer
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:LAYer:RCONfig
	single: CONFigure:CDMA:SIGNaling<Instance>:LAYer:MODulation

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:LAYer:RCONfig
	CONFigure:CDMA:SIGNaling<Instance>:LAYer:MODulation



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Layer.Layer
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.layer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Layer_Soption.rst
	Configure_Layer_Channel.rst
	Configure_Layer_Fch.rst
	Configure_Layer_Sch.rst
	Configure_Layer_Pch.rst
	Configure_Layer_Qpch.rst