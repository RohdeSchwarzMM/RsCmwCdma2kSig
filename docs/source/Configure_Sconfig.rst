Sconfig
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:SCONfig:AMOC
	single: CONFigure:CDMA:SIGNaling<Instance>:SCONfig:APCalls

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:SCONfig:AMOC
	CONFigure:CDMA:SIGNaling<Instance>:SCONfig:APCalls



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Sconfig.Sconfig
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.sconfig.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Sconfig_Loop.rst
	Configure_Sconfig_Speech.rst
	Configure_Sconfig_Tdata.rst
	Configure_Sconfig_Pdata.rst