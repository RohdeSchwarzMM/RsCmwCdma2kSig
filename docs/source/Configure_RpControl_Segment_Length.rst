Length
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:RPControl:SEGMent<Segment>:LENGth

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:RPControl:SEGMent<Segment>:LENGth



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.RpControl_.Segment_.Length.Length
	:members:
	:undoc-members:
	:noindex: