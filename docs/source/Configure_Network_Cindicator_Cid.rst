Cid
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:CINDicator:CID:ENABle
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:CINDicator:CID:PINDicator
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:CINDicator:CID

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:NETWork:CINDicator:CID:ENABle
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:CINDicator:CID:PINDicator
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:CINDicator:CID



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Network_.Cindicator_.Cid.Cid
	:members:
	:undoc-members:
	:noindex: