Configure
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:DISPlay
	single: CONFigure:CDMA:SIGNaling<Instance>:ETOE
	single: CONFigure:CDMA:SIGNaling<Instance>:ESCode

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:DISPlay
	CONFigure:CDMA:SIGNaling<Instance>:ETOE
	CONFigure:CDMA:SIGNaling<Instance>:ESCode



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure.Configure
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Test.rst
	Configure_RfSettings.rst
	Configure_Fading.rst
	Configure_IqIn.rst
	Configure_Mmonitor.rst
	Configure_Cstatus.rst
	Configure_RfPower.rst
	Configure_Layer.rst
	Configure_RpControl.rst
	Configure_System.rst
	Configure_Sconfig.rst
	Configure_Network.rst
	Configure_Connection.rst
	Configure_MsInfo.rst
	Configure_Capabilities.rst
	Configure_Handoff.rst
	Configure_Reconfigure.rst
	Configure_Preconfigure.rst
	Configure_Sms.rst
	Configure_RxQuality.rst