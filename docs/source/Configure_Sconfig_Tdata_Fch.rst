Fch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:SCONfig:TDATa:FCH:PGENeration
	single: CONFigure:CDMA:SIGNaling<Instance>:SCONfig:TDATa:FCH:PATTern
	single: CONFigure:CDMA:SIGNaling<Instance>:SCONfig:TDATa:FCH:CBFRames
	single: CONFigure:CDMA:SIGNaling<Instance>:SCONfig:TDATa:FCH:TXON
	single: CONFigure:CDMA:SIGNaling<Instance>:SCONfig:TDATa:FCH:TXOFf

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:SCONfig:TDATa:FCH:PGENeration
	CONFigure:CDMA:SIGNaling<Instance>:SCONfig:TDATa:FCH:PATTern
	CONFigure:CDMA:SIGNaling<Instance>:SCONfig:TDATa:FCH:CBFRames
	CONFigure:CDMA:SIGNaling<Instance>:SCONfig:TDATa:FCH:TXON
	CONFigure:CDMA:SIGNaling<Instance>:SCONfig:TDATa:FCH:TXOFf



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Sconfig_.Tdata_.Fch.Fch
	:members:
	:undoc-members:
	:noindex: