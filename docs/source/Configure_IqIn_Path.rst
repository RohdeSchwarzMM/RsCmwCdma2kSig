Path<Path>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.iqIn.path.repcap_path_get()
	driver.configure.iqIn.path.repcap_path_set(repcap.Path.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:IQIN:PATH<Path>

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:IQIN:PATH<Path>



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.IqIn_.Path.Path
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.iqIn.path.clone()