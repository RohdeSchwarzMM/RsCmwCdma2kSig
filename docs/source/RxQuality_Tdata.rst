Tdata
----------------------------------------





.. autoclass:: RsCmwCdma2kSig.Implementations.RxQuality_.Tdata.Tdata
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.tdata.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_Tdata_FerfCh.rst
	RxQuality_Tdata_FersCh.rst