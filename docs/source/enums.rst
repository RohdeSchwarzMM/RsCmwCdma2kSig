Enums
=========

AcceptState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AcceptState.ACCept
	# All values (2x):
	ACCept | REJect

AccessProbeMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AccessProbeMode.ACK
	# All values (2x):
	ACK | IGN

AckState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AckState.ACK
	# All values (2x):
	ACK | NACK

ApplyTimeAt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ApplyTimeAt.EVER
	# All values (3x):
	EVER | NEXT | SUSO

AvgEncodingRate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AvgEncodingRate.R48K
	# All values (8x):
	R48K | R58K | R62K | R66K | R70K | R75K | R85K | R93K

BandClass
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.BandClass.AWS
	# Last value:
	value = enums.BandClass.USPC
	# All values (23x):
	AWS | B18M | IEXT | IM2K | JTAC | KCEL | KPCS | LBANd
	LO7C | N45T | NA7C | NA8S | NA9C | NAPC | PA4M | PA8M
	PS7C | SBANd | TACS | U25B | U25F | USC | USPC

CallerIdPresentation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CallerIdPresentation.NNAV
	# All values (3x):
	NNAV | PAL | PRES

CsAction
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CsAction.BROadcast
	# All values (6x):
	BROadcast | CONNect | DISConnect | HANDoff | SMS | UNRegister

CsState
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.CsState.ALERting
	# Last value:
	value = enums.CsState.SENDing
	# All values (9x):
	ALERting | BROadcast | CONNected | IDLE | OFF | ON | PAGing | REGistered
	SENDing

DeliveryStatus
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DeliveryStatus.ACKTimeout
	# All values (5x):
	ACKTimeout | BADData | CSTate | PENDing | SUCCess

DeviceType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DeviceType.FULL
	# All values (3x):
	FULL | LIMited | NO

DirectionHorizontal
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DirectionHorizontal.EAST
	# All values (2x):
	EAST | WEST

DirectionVertical
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DirectionVertical.NORTh
	# All values (2x):
	NORTh | SOUTh

DisplayTab
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DisplayTab.FERFch
	# All values (5x):
	FERFch | FERSch0 | POWer | RLP | SPEech

ExpectedPowerMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ExpectedPowerMode.MANual
	# All values (4x):
	MANual | MAX | MIN | OLRule

FadingSimRestartMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FadingSimRestartMode.AUTO
	# All values (3x):
	AUTO | MANual | TRIGger

FadingSimStandard
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FadingSimStandard.P1
	# All values (6x):
	P1 | P2 | P3 | P4 | P5 | P6

ForwardCoding
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ForwardCoding.CONV
	# All values (2x):
	CONV | TURB

ForwardDataRate
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ForwardDataRate.R115k
	# Last value:
	value = enums.ForwardDataRate.R9K
	# All values (10x):
	R115k | R14K | R153k | R19K | R230k | R28K | R38K | R57K
	R76K | R9K

ForwardFrameType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ForwardFrameType.R1
	# All values (2x):
	R1 | R2

FrameRate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FrameRate.EIGHth
	# All values (4x):
	EIGHth | FULL | HALF | QUARter

GeoLocationType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GeoLocationType.AAG
	# All values (4x):
	AAG | AFLT | GPS | NSUP

HookStatus
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HookStatus.OFF
	# All values (3x):
	OFF | ON | SOFF

InsertLossMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InsertLossMode.LACP
	# All values (3x):
	LACP | NORMal | USER

IpAddressIndex
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IpAddressIndex.IP1
	# All values (3x):
	IP1 | IP2 | IP3

KeepConstant
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.KeepConstant.DSHift
	# All values (2x):
	DSHift | SPEed

Language
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Language.AFRikaans
	# Last value:
	value = enums.Language.VIETnamese
	# All values (41x):
	AFRikaans | ARABic | BAHasa | BENGali | CHINese | CZECh | DANish | DUTCh
	ENGLish | FINNish | FRENch | GERMan | GREek | GUJarati | HAUSa | HEBRew
	HINDi | HUNGarian | ICELandic | ITALian | JAPanese | KANNada | KORean | MALayalam
	NORWegian | ORIYa | POLish | PORTuguese | PUNJabi | RUSSian | SPANish | SWAHili
	SWEDish | TAGalog | TAMil | TELugu | THAI | TURKish | UNDefined | URDU
	VIETnamese

LogCategory
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LogCategory.CONTinue
	# All values (4x):
	CONTinue | ERRor | INFO | WARNing

LongSmsHandling
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LongSmsHandling.MSMS
	# All values (2x):
	MSMS | TRUNcate

MainState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MainState.OFF
	# All values (3x):
	OFF | ON | RFHandover

MessageHandling
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MessageHandling.FILE
	# All values (2x):
	FILE | INTernal

MocCallsAcceptMode
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.MocCallsAcceptMode.ALL
	# Last value:
	value = enums.MocCallsAcceptMode.SCL1
	# All values (13x):
	ALL | BUAW | BUFW | FSC1 | ICAW | ICFW | ICOR | IGNR
	RERO | ROAW | ROFW | ROOR | SCL1

Modulation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Modulation.HPSK
	# All values (2x):
	HPSK | QPSK

NetworkSegment
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NetworkSegment.A
	# All values (3x):
	A | B | C

OtaspSendMethodA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OtaspSendMethodA.NONE
	# All values (3x):
	NONE | SO18 | SO19

OtaspSendMethodB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OtaspSendMethodB.NONE
	# All values (4x):
	NONE | SO18 | SO19 | TCH

PagingChannelRate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PagingChannelRate.R4K8
	# All values (2x):
	R4K8 | R9K6

PatternGeneration
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PatternGeneration.FIX
	# All values (2x):
	FIX | RAND

PdmSendMethodA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PdmSendMethodA.NONE
	# All values (4x):
	NONE | PCH | SO35 | SO36

PdmSendMethodB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PdmSendMethodB.NONE
	# All values (5x):
	NONE | PCH | SO35 | SO36 | TCH

PlcmDerivation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PlcmDerivation.ESN
	# All values (2x):
	ESN | MEID

PnChips
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.PnChips.C10
	# Last value:
	value = enums.PnChips.C80
	# All values (16x):
	C10 | C100 | C130 | C14 | C160 | C20 | C226 | C28
	C320 | C4 | C40 | C452 | C6 | C60 | C8 | C80

PowerCtrlBits
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowerCtrlBits.ADOWn
	# All values (6x):
	ADOWn | AUP | AUTO | HOLD | PATTern | RTESt

PriorityB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PriorityB.EMERgency
	# All values (4x):
	EMERgency | INTeractive | NORMal | URGent

QueueState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.QueueState.OK
	# All values (2x):
	OK | OVERflow

RadioConfig
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RadioConfig.F1R1
	# All values (5x):
	F1R1 | F2R2 | F3R3 | F4R3 | F5R4

RateRestriction
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RateRestriction.AUTO
	# All values (5x):
	AUTO | EIGHth | FULL | HALF | QUARter

RegistrationType
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RegistrationType.DISTance
	# Last value:
	value = enums.RegistrationType.ZONE
	# All values (10x):
	DISTance | IMPLicit | IORM | ORDered | PARChange | PWDown | PWUP | TIMer
	USEZone | ZONE

Repeat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Repeat.CONTinuous
	# All values (2x):
	CONTinuous | SINGleshot

ResourceState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceState.ACTive
	# All values (8x):
	ACTive | ADJusted | INValid | OFF | PENDing | QUEued | RDY | RUN

RxConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConnector.I11I
	# Last value:
	value = enums.RxConnector.RH8
	# All values (154x):
	I11I | I13I | I15I | I17I | I21I | I23I | I25I | I27I
	I31I | I33I | I35I | I37I | I41I | I43I | I45I | I47I
	IF1 | IF2 | IF3 | IQ1I | IQ3I | IQ5I | IQ7I | R11
	R11C | R12 | R12C | R12I | R13 | R13C | R14 | R14C
	R14I | R15 | R16 | R17 | R18 | R21 | R21C | R22
	R22C | R22I | R23 | R23C | R24 | R24C | R24I | R25
	R26 | R27 | R28 | R31 | R31C | R32 | R32C | R32I
	R33 | R33C | R34 | R34C | R34I | R35 | R36 | R37
	R38 | R41 | R41C | R42 | R42C | R42I | R43 | R43C
	R44 | R44C | R44I | R45 | R46 | R47 | R48 | RA1
	RA2 | RA3 | RA4 | RA5 | RA6 | RA7 | RA8 | RB1
	RB2 | RB3 | RB4 | RB5 | RB6 | RB7 | RB8 | RC1
	RC2 | RC3 | RC4 | RC5 | RC6 | RC7 | RC8 | RD1
	RD2 | RD3 | RD4 | RD5 | RD6 | RD7 | RD8 | RE1
	RE2 | RE3 | RE4 | RE5 | RE6 | RE7 | RE8 | RF1
	RF1C | RF2 | RF2C | RF2I | RF3 | RF3C | RF4 | RF4C
	RF4I | RF5 | RF5C | RF6 | RF6C | RF7 | RF8 | RFAC
	RFBC | RFBI | RG1 | RG2 | RG3 | RG4 | RG5 | RG6
	RG7 | RG8 | RH1 | RH2 | RH3 | RH4 | RH5 | RH6
	RH7 | RH8

RxConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConverter.IRX1
	# Last value:
	value = enums.RxConverter.RX44
	# All values (40x):
	IRX1 | IRX11 | IRX12 | IRX13 | IRX14 | IRX2 | IRX21 | IRX22
	IRX23 | IRX24 | IRX3 | IRX31 | IRX32 | IRX33 | IRX34 | IRX4
	IRX41 | IRX42 | IRX43 | IRX44 | RX1 | RX11 | RX12 | RX13
	RX14 | RX2 | RX21 | RX22 | RX23 | RX24 | RX3 | RX31
	RX32 | RX33 | RX34 | RX4 | RX41 | RX42 | RX43 | RX44

Scenario
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Scenario.HMFading
	# All values (6x):
	HMFading | HMLite | HMODe | SCELl | SCFading | UNDefined

SegmentBits
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SegmentBits.ALTernating
	# All values (3x):
	ALTernating | DOWN | UP

ServiceOption
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ServiceOption.SO1
	# Last value:
	value = enums.ServiceOption.SO9
	# All values (12x):
	SO1 | SO17 | SO2 | SO3 | SO32 | SO33 | SO55 | SO68
	SO70 | SO73 | SO8000 | SO9

SmsSendMethod
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SmsSendMethod.ACH
	# All values (5x):
	ACH | PCH | SO14 | SO6 | TCH

SourceInt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SourceInt.EXTernal
	# All values (2x):
	EXTernal | INTernal

StopConditionB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StopConditionB.ALEXeeded
	# All values (4x):
	ALEXeeded | MCLexceeded | MFER | NONE

Supported
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Supported.NSUP
	# All values (2x):
	NSUP | SUPP

SyncState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SyncState.ADINtermed
	# All values (7x):
	ADINtermed | ADJusted | INValid | OFF | ON | PENDing | RFHandover

TimeSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TimeSource.CMWTime
	# All values (3x):
	CMWTime | DATE | SYNC

TxConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TxConnector.I12O
	# Last value:
	value = enums.TxConnector.RH18
	# All values (77x):
	I12O | I14O | I16O | I18O | I22O | I24O | I26O | I28O
	I32O | I34O | I36O | I38O | I42O | I44O | I46O | I48O
	IF1 | IF2 | IF3 | IQ2O | IQ4O | IQ6O | IQ8O | R118
	R1183 | R1184 | R11C | R11O | R11O3 | R11O4 | R12C | R13C
	R13O | R14C | R214 | R218 | R21C | R21O | R22C | R23C
	R23O | R24C | R258 | R318 | R31C | R31O | R32C | R33C
	R33O | R34C | R418 | R41C | R41O | R42C | R43C | R43O
	R44C | RA18 | RB14 | RB18 | RC18 | RD18 | RE18 | RF18
	RF1C | RF1O | RF2C | RF3C | RF3O | RF4C | RF5C | RF6C
	RFAC | RFAO | RFBC | RG18 | RH18

TxConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TxConverter.ITX1
	# Last value:
	value = enums.TxConverter.TX44
	# All values (40x):
	ITX1 | ITX11 | ITX12 | ITX13 | ITX14 | ITX2 | ITX21 | ITX22
	ITX23 | ITX24 | ITX3 | ITX31 | ITX32 | ITX33 | ITX34 | ITX4
	ITX41 | ITX42 | ITX43 | ITX44 | TX1 | TX11 | TX12 | TX13
	TX14 | TX2 | TX21 | TX22 | TX23 | TX24 | TX3 | TX31
	TX32 | TX33 | TX34 | TX4 | TX41 | TX42 | TX43 | TX44

VoiceCoder
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.VoiceCoder.CODE
	# All values (2x):
	CODE | ECHO

YesNoStatus
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.YesNoStatus.NO
	# All values (2x):
	NO | YES

