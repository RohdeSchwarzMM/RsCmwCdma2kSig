Fch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:LAYer:FCH:FOFFset

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:LAYer:FCH:FOFFset



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Layer_.Fch.Fch
	:members:
	:undoc-members:
	:noindex: