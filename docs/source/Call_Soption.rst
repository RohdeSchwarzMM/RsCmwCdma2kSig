Soption
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALL:CDMA:SIGNaling<Instance>:SOPTion<Const_ServiceOption>:ACTion

.. code-block:: python

	CALL:CDMA:SIGNaling<Instance>:SOPTion<Const_ServiceOption>:ACTion



.. autoclass:: RsCmwCdma2kSig.Implementations.Call_.Soption.Soption
	:members:
	:undoc-members:
	:noindex: