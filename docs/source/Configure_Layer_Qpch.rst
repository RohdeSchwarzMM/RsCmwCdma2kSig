Qpch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:LAYer:QPCH:CHANnel
	single: CONFigure:CDMA:SIGNaling<Instance>:LAYer:QPCH:LEVel
	single: CONFigure:CDMA:SIGNaling<Instance>:LAYer:QPCH:RATE

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:LAYer:QPCH:CHANnel
	CONFigure:CDMA:SIGNaling<Instance>:LAYer:QPCH:LEVel
	CONFigure:CDMA:SIGNaling<Instance>:LAYer:QPCH:RATE



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Layer_.Qpch.Qpch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.layer.qpch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Layer_Qpch_Ibit.rst