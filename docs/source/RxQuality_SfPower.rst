SfPower
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:SIGNaling<Instance>:RXQuality:SFPower
	single: FETCh:CDMA:SIGNaling<Instance>:RXQuality:SFPower

.. code-block:: python

	READ:CDMA:SIGNaling<Instance>:RXQuality:SFPower
	FETCh:CDMA:SIGNaling<Instance>:RXQuality:SFPower



.. autoclass:: RsCmwCdma2kSig.Implementations.RxQuality_.SfPower.SfPower
	:members:
	:undoc-members:
	:noindex: