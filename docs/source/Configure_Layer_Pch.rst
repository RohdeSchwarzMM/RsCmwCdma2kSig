Pch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:LAYer:PCH:CHANnel
	single: CONFigure:CDMA:SIGNaling<Instance>:LAYer:PCH:LEVel
	single: CONFigure:CDMA:SIGNaling<Instance>:LAYer:PCH:RATE

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:LAYer:PCH:CHANnel
	CONFigure:CDMA:SIGNaling<Instance>:LAYer:PCH:LEVel
	CONFigure:CDMA:SIGNaling<Instance>:LAYer:PCH:RATE



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Layer_.Pch.Pch
	:members:
	:undoc-members:
	:noindex: