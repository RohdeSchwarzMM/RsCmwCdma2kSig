Send
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALL:CDMA:SIGNaling<Instance>:PDM:SEND:TRANsmit
	single: CALL:CDMA:SIGNaling<Instance>:PDM:SEND:MODE
	single: CALL:CDMA:SIGNaling<Instance>:PDM:SEND:STATus

.. code-block:: python

	CALL:CDMA:SIGNaling<Instance>:PDM:SEND:TRANsmit
	CALL:CDMA:SIGNaling<Instance>:PDM:SEND:MODE
	CALL:CDMA:SIGNaling<Instance>:PDM:SEND:STATus



.. autoclass:: RsCmwCdma2kSig.Implementations.Call_.Pdm_.Send.Send
	:members:
	:undoc-members:
	:noindex: