Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:RXQuality:RESult:FERFch
	single: CONFigure:CDMA:SIGNaling<Instance>:RXQuality:RESult:FERSch
	single: CONFigure:CDMA:SIGNaling<Instance>:RXQuality:RESult:RLP
	single: CONFigure:CDMA:SIGNaling<Instance>:RXQuality:RESult:SPEech
	single: CONFigure:CDMA:SIGNaling<Instance>:RXQuality:RESult:PSTRength

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:RXQuality:RESult:FERFch
	CONFigure:CDMA:SIGNaling<Instance>:RXQuality:RESult:FERSch
	CONFigure:CDMA:SIGNaling<Instance>:RXQuality:RESult:RLP
	CONFigure:CDMA:SIGNaling<Instance>:RXQuality:RESult:SPEech
	CONFigure:CDMA:SIGNaling<Instance>:RXQuality:RESult:PSTRength



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.RxQuality_.Result.Result
	:members:
	:undoc-members:
	:noindex: