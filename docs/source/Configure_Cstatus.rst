Cstatus
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:CSTatus:LOG
	single: CONFigure:CDMA:SIGNaling<Instance>:CSTatus:VCODer

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:CSTatus:LOG
	CONFigure:CDMA:SIGNaling<Instance>:CSTatus:VCODer



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Cstatus.Cstatus
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cstatus.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cstatus_Moption.rst
	Configure_Cstatus_Drate.rst