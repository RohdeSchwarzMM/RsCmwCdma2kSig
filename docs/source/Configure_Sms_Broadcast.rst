Broadcast
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:SMS:BROadcast:CMAS
	single: CONFigure:CDMA:SIGNaling<Instance>:SMS:BROadcast:WEA
	single: CONFigure:CDMA:SIGNaling<Instance>:SMS:BROadcast:INTernal
	single: CONFigure:CDMA:SIGNaling<Instance>:SMS:BROadcast:LANGuage
	single: CONFigure:CDMA:SIGNaling<Instance>:SMS:BROadcast:PRIority

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:SMS:BROadcast:CMAS
	CONFigure:CDMA:SIGNaling<Instance>:SMS:BROadcast:WEA
	CONFigure:CDMA:SIGNaling<Instance>:SMS:BROadcast:INTernal
	CONFigure:CDMA:SIGNaling<Instance>:SMS:BROadcast:LANGuage
	CONFigure:CDMA:SIGNaling<Instance>:SMS:BROadcast:PRIority



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Sms_.Broadcast.Broadcast
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.sms.broadcast.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Sms_Broadcast_Service.rst