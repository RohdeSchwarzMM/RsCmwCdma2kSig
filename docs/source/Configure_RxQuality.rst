RxQuality
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:RXQuality:URATe
	single: CONFigure:CDMA:SIGNaling<Instance>:RXQuality:WINDowsize

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:RXQuality:URATe
	CONFigure:CDMA:SIGNaling<Instance>:RXQuality:WINDowsize



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.RxQuality.RxQuality
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rxQuality.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RxQuality_Result.rst
	Configure_RxQuality_FerfCh.rst
	Configure_RxQuality_FersCh.rst
	Configure_RxQuality_Rstatistics.rst
	Configure_RxQuality_Pstrength.rst
	Configure_RxQuality_Limit.rst