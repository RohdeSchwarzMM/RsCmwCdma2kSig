RpControl
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:RPControl:PCBits
	single: CONFigure:CDMA:SIGNaling<Instance>:RPControl:SSIZe
	single: CONFigure:CDMA:SIGNaling<Instance>:RPControl:REPetition
	single: CONFigure:CDMA:SIGNaling<Instance>:RPControl:RUN

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:RPControl:PCBits
	CONFigure:CDMA:SIGNaling<Instance>:RPControl:SSIZe
	CONFigure:CDMA:SIGNaling<Instance>:RPControl:REPetition
	CONFigure:CDMA:SIGNaling<Instance>:RPControl:RUN



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.RpControl.RpControl
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rpControl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RpControl_Segment.rst