FersCh
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:RXQuality:LIMit:FERSch:MFER
	single: CONFigure:CDMA:SIGNaling<Instance>:RXQuality:LIMit:FERSch:CLEVel

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:RXQuality:LIMit:FERSch:MFER
	CONFigure:CDMA:SIGNaling<Instance>:RXQuality:LIMit:FERSch:CLEVel



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.RxQuality_.Limit_.FersCh.FersCh
	:members:
	:undoc-members:
	:noindex: