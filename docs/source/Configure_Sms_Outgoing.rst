Outgoing
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:SMS:OUTGoing:SMEThod
	single: CONFigure:CDMA:SIGNaling<Instance>:SMS:OUTGoing:ACKNowledge
	single: CONFigure:CDMA:SIGNaling<Instance>:SMS:OUTGoing:ATSTamp
	single: CONFigure:CDMA:SIGNaling<Instance>:SMS:OUTGoing:LHANdling
	single: CONFigure:CDMA:SIGNaling<Instance>:SMS:OUTGoing:MESHandling
	single: CONFigure:CDMA:SIGNaling<Instance>:SMS:OUTGoing:INTernal

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:SMS:OUTGoing:SMEThod
	CONFigure:CDMA:SIGNaling<Instance>:SMS:OUTGoing:ACKNowledge
	CONFigure:CDMA:SIGNaling<Instance>:SMS:OUTGoing:ATSTamp
	CONFigure:CDMA:SIGNaling<Instance>:SMS:OUTGoing:LHANdling
	CONFigure:CDMA:SIGNaling<Instance>:SMS:OUTGoing:MESHandling
	CONFigure:CDMA:SIGNaling<Instance>:SMS:OUTGoing:INTernal



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Sms_.Outgoing.Outgoing
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.sms.outgoing.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Sms_Outgoing_File.rst