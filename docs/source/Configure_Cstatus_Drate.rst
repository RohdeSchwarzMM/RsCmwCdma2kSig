Drate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:CSTatus:DRATe:SCH

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:CSTatus:DRATe:SCH



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Cstatus_.Drate.Drate
	:members:
	:undoc-members:
	:noindex: