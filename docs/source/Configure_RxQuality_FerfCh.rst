FerfCh
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:RXQuality:FERFch:TOUT
	single: CONFigure:CDMA:SIGNaling<Instance>:RXQuality:FERFch:REPetition
	single: CONFigure:CDMA:SIGNaling<Instance>:RXQuality:FERFch:SCONdition
	single: CONFigure:CDMA:SIGNaling<Instance>:RXQuality:FERFch:FRAMes

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:RXQuality:FERFch:TOUT
	CONFigure:CDMA:SIGNaling<Instance>:RXQuality:FERFch:REPetition
	CONFigure:CDMA:SIGNaling<Instance>:RXQuality:FERFch:SCONdition
	CONFigure:CDMA:SIGNaling<Instance>:RXQuality:FERFch:FRAMes



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.RxQuality_.FerfCh.FerfCh
	:members:
	:undoc-members:
	:noindex: