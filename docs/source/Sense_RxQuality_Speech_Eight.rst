Eight
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:SPEech:EIGHt:PERCent
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:SPEech:EIGHt

.. code-block:: python

	SENSe:CDMA:SIGNaling<Instance>:RXQuality:SPEech:EIGHt:PERCent
	SENSe:CDMA:SIGNaling<Instance>:RXQuality:SPEech:EIGHt



.. autoclass:: RsCmwCdma2kSig.Implementations.Sense_.RxQuality_.Speech_.Eight.Eight
	:members:
	:undoc-members:
	:noindex: