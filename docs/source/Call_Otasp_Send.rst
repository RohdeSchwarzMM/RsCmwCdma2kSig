Send
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALL:CDMA:SIGNaling<Instance>:OTASp:SEND:TRANsmit
	single: CALL:CDMA:SIGNaling<Instance>:OTASp:SEND:MODE
	single: CALL:CDMA:SIGNaling<Instance>:OTASp:SEND:STATus

.. code-block:: python

	CALL:CDMA:SIGNaling<Instance>:OTASp:SEND:TRANsmit
	CALL:CDMA:SIGNaling<Instance>:OTASp:SEND:MODE
	CALL:CDMA:SIGNaling<Instance>:OTASp:SEND:STATus



.. autoclass:: RsCmwCdma2kSig.Implementations.Call_.Otasp_.Send.Send
	:members:
	:undoc-members:
	:noindex: