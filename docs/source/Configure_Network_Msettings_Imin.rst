Imin
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:MSETtings:IMIN:USER

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:NETWork:MSETtings:IMIN:USER



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Network_.Msettings_.Imin.Imin
	:members:
	:undoc-members:
	:noindex: