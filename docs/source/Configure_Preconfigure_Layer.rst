Layer
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:PREConfigure:LAYer:RCONfig

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:PREConfigure:LAYer:RCONfig



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Preconfigure_.Layer.Layer
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.preconfigure.layer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Preconfigure_Layer_Soption.rst