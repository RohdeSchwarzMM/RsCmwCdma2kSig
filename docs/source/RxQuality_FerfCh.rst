FerfCh
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:CDMA:SIGNaling<Instance>:RXQuality:FERFch
	single: FETCh:CDMA:SIGNaling<Instance>:RXQuality:FERFch
	single: CALCulate:CDMA:SIGNaling<Instance>:RXQuality:FERFch

.. code-block:: python

	READ:CDMA:SIGNaling<Instance>:RXQuality:FERFch
	FETCh:CDMA:SIGNaling<Instance>:RXQuality:FERFch
	CALCulate:CDMA:SIGNaling<Instance>:RXQuality:FERFch



.. autoclass:: RsCmwCdma2kSig.Implementations.RxQuality_.FerfCh.FerfCh
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.ferfCh.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_FerfCh_Tdata.rst
	RxQuality_FerfCh_State.rst