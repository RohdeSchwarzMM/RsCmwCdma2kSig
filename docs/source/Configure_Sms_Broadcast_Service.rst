Service
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:SMS:BROadcast:SERVice:CATegory

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:SMS:BROadcast:SERVice:CATegory



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Sms_.Broadcast_.Service.Service
	:members:
	:undoc-members:
	:noindex: