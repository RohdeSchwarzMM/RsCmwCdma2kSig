Pchannel
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:PCHannel:RATE
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:PCHannel:SCINdex
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:PCHannel:MSCindex
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:PCHannel:BSCindex
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:PCHannel:PRMS

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:NETWork:PCHannel:RATE
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:PCHannel:SCINdex
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:PCHannel:MSCindex
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:PCHannel:BSCindex
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:PCHannel:PRMS



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Network_.Pchannel.Pchannel
	:members:
	:undoc-members:
	:noindex: