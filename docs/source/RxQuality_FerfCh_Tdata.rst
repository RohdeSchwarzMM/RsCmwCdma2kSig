Tdata
----------------------------------------





.. autoclass:: RsCmwCdma2kSig.Implementations.RxQuality_.FerfCh_.Tdata.Tdata
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.ferfCh.tdata.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_FerfCh_Tdata_State.rst