Sch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:LAYer:SCH:FOFFset
	single: CONFigure:CDMA:SIGNaling<Instance>:LAYer:SCH:MPPL
	single: CONFigure:CDMA:SIGNaling<Instance>:LAYer:SCH:FTYPe
	single: CONFigure:CDMA:SIGNaling<Instance>:LAYer:SCH:DRATe
	single: CONFigure:CDMA:SIGNaling<Instance>:LAYer:SCH:FSIZe
	single: CONFigure:CDMA:SIGNaling<Instance>:LAYer:SCH:CODing

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:LAYer:SCH:FOFFset
	CONFigure:CDMA:SIGNaling<Instance>:LAYer:SCH:MPPL
	CONFigure:CDMA:SIGNaling<Instance>:LAYer:SCH:FTYPe
	CONFigure:CDMA:SIGNaling<Instance>:LAYer:SCH:DRATe
	CONFigure:CDMA:SIGNaling<Instance>:LAYer:SCH:FSIZe
	CONFigure:CDMA:SIGNaling<Instance>:LAYer:SCH:CODing



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Layer_.Sch.Sch
	:members:
	:undoc-members:
	:noindex: