Sch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:LAYer:CHANnel:SCH

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:LAYer:CHANnel:SCH



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Layer_.Channel_.Sch.Sch
	:members:
	:undoc-members:
	:noindex: