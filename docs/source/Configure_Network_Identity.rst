Identity
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:IDENtity:NID
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:IDENtity:MCC
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:IDENtity:IMSI
	single: CONFigure:CDMA:SIGNaling<Instance>:NETWork:IDENtity:UWCard

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:NETWork:IDENtity:NID
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:IDENtity:MCC
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:IDENtity:IMSI
	CONFigure:CDMA:SIGNaling<Instance>:NETWork:IDENtity:UWCard



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Network_.Identity.Identity
	:members:
	:undoc-members:
	:noindex: