Bits
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:RPControl:SEGMent<Segment>:BITS

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:RPControl:SEGMent<Segment>:BITS



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.RpControl_.Segment_.Bits.Bits
	:members:
	:undoc-members:
	:noindex: