Info
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:SMS:INFO:LSMessage

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:SMS:INFO:LSMessage



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Sms_.Info.Info
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.sms.info.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Sms_Info_LrMessage.rst