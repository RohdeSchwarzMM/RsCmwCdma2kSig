Bandwidth
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:CDMA:SIGNaling<Instance>:FADing:AWGN:BWIDth:RATio
	single: CONFigure:CDMA:SIGNaling<Instance>:FADing:AWGN:BWIDth:NOISe

.. code-block:: python

	CONFigure:CDMA:SIGNaling<Instance>:FADing:AWGN:BWIDth:RATio
	CONFigure:CDMA:SIGNaling<Instance>:FADing:AWGN:BWIDth:NOISe



.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Fading_.Awgn_.Bandwidth.Bandwidth
	:members:
	:undoc-members:
	:noindex: