HmFading
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:CDMA:SIGNaling<Instance>:SCENario:HMFading:EXTernal
	single: ROUTe:CDMA:SIGNaling<Instance>:SCENario:HMFading:INTernal

.. code-block:: python

	ROUTe:CDMA:SIGNaling<Instance>:SCENario:HMFading:EXTernal
	ROUTe:CDMA:SIGNaling<Instance>:SCENario:HMFading:INTernal



.. autoclass:: RsCmwCdma2kSig.Implementations.Route_.Scenario_.HmFading.HmFading
	:members:
	:undoc-members:
	:noindex: