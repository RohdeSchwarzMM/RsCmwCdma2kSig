Tdata
----------------------------------------





.. autoclass:: RsCmwCdma2kSig.Implementations.Configure_.Sconfig_.Tdata.Tdata
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.sconfig.tdata.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Sconfig_Tdata_Fch.rst
	Configure_Sconfig_Tdata_Sch.rst