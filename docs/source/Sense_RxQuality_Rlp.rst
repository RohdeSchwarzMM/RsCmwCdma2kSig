Rlp
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:DUNSegmented
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:DSEGmented
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:FILL
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:IDLE
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:NAK
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:SYNC
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:ACK
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:SACK
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:BDATa
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:CDATa
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:DDATa
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:REASembly
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:BLANk
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:INValid
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:SUMMary
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:PPPTotal
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:DRATe
	single: SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:STATe

.. code-block:: python

	SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:DUNSegmented
	SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:DSEGmented
	SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:FILL
	SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:IDLE
	SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:NAK
	SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:SYNC
	SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:ACK
	SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:SACK
	SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:BDATa
	SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:CDATa
	SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:DDATa
	SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:REASembly
	SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:BLANk
	SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:INValid
	SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:SUMMary
	SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:PPPTotal
	SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:DRATe
	SENSe:CDMA:SIGNaling<Instance>:RXQuality:RLP:STATe



.. autoclass:: RsCmwCdma2kSig.Implementations.Sense_.RxQuality_.Rlp.Rlp
	:members:
	:undoc-members:
	:noindex: