"""RsCmwCdma2kSig instrument driver
	:version: 3.8.10.25
	:copyright: 2021 by Rohde & Schwarz GMBH & Co. KG
	:license: MIT, see LICENSE for more details.
"""

__version__ = '3.8.10.25'

# Main class
from RsCmwCdma2kSig.RsCmwCdma2kSig import RsCmwCdma2kSig

# Bin data format
from RsCmwCdma2kSig.Internal.Conversions import BinIntFormat, BinFloatFormat

# Exceptions
from RsCmwCdma2kSig.Internal.InstrumentErrors import RsInstrException, TimeoutException, StatusException, UnexpectedResponseException, ResourceError, DriverValueError

# Callback Event Argument prototypes
from RsCmwCdma2kSig.Internal.IoTransferEventArgs import IoTransferEventArgs

# enums
from RsCmwCdma2kSig import enums

# repcaps
from RsCmwCdma2kSig import repcap

# Reliability interface
from RsCmwCdma2kSig.CustomFiles.reliability import Reliability, ReliabilityEventArgs, codes_table
